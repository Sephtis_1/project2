﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lifespan : MonoBehaviour
{

    private SpriteRenderer sr;
    int targetvalue;
    int health;

    // Use this for initialization
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        targetvalue = 0;
        health = 5;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (health > targetvalue)
        {
            --health;
        }
        if (health == targetvalue)
        {
            Destroy(gameObject);
        }
    }
}
    