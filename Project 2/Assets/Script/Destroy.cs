﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour {

    private SpriteRenderer sr;
    int targetvalue;
    int life;
    public Transform tf;

    // Use this for initialization
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        targetvalue = 0;
        life = 3;
        tf = GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (life > targetvalue)
        {
            Vector3 startPos = default(Vector3);
            tf.position = startPos;
            --life;
        }
        if (life == targetvalue)
        {
            Destroy(gameObject);
            Application.Quit();
        }
    }
    void OnCollisionEnter2D()
    {
        Debug.Log("You have been DESTROYED");
    }
}
