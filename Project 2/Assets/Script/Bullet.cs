﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public Transform tf;
	public float speed;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}
	public void OnTriggerEnter2D (Collider2D otherCollider){
		Destroy (otherCollider.gameObject);
		Destroy (gameObject);
	}

	public void Move (){
		tf.position += tf.right * speed;
	}
}
