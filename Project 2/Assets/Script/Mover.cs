﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
	public Transform  tf;
	public float speed;
	public float turnspeed;

	// Use this for initialization
	void Start ()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetKey (KeyCode.W)) {
			tf.position = tf.position + tf.right;
			//tf.Rotate (0,0,turnspeed);

        }
		
		if (Input.GetKey (KeyCode.A)) {
			//tf.position = tf.position + tf.up;
			tf.Rotate (0,0,turnspeed);
        }
		
		if (Input.GetKey (KeyCode.D)) {
           // tf.position = tf.position - tf.up;
		    tf.Rotate (0,0, -turnspeed);
        }
		
		if (Input.GetKey (KeyCode.S)) {
           // tf.position = tf.position - tf.right;
			//tf.Rotate (0,0,turnspeed);
        }
	}
}
