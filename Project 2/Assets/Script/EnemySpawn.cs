﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject shipPrefab;
    public GameObject asteroidPrefab;
    public Transform asteroidSpawn;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Instantiate(shipPrefab, asteroidSpawn.position, asteroidSpawn.rotation);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            Instantiate(asteroidPrefab, asteroidSpawn.position, asteroidSpawn.rotation);
        }
    }
}
