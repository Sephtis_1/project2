﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager instance;
    public int score = 0;
    void Awake()
    {
        // As long as there is not an instance already set
        if (instance == null)
        {
            instance = this; 
            DontDestroyOnLoad(gameObject); 
        }
        else
        {
            Destroy(this.gameObject); 
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }
    }
}
